## Installing
`npm install`

## Running
`npm start`

The script should launch your default browser automatically. If not, the local
web page can be accessed at http://localhost:3000
