import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const formatcoords = require('formatcoords');

const Observation = ({ name, rarity, notes, timestamp, geolocation}) => {
  const date = new Date(timestamp).toLocaleString('en-GB');

  let geolocationJsx;
  if (geolocation)  {
    geolocationJsx = (
      <Typography>
        {formatcoords(geolocation[0], geolocation[1]).format()}
      </Typography>
    );
  }

  return(
    <Card style={{ height: "100%" }}>
     <CardContent>
       <Typography variant="h5" component="h2">
         {name}
       </Typography>
       <Typography color="textSecondary">
         {rarity}
       </Typography>
       <Typography component="p">
         {notes}
       </Typography>
       <br />
       {geolocationJsx}
       <Typography>
         {date}
       </Typography>
     </CardContent>
   </Card>
  );
};

export default Observation;
