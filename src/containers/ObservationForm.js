import React, { Component } from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import FilledInput from '@material-ui/core/FilledInput';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link, Redirect } from 'react-router-dom';

const uuidv4 = require('uuid/v4');

class ObservationForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      submitted: false,
      name: '',
      rarity: 'common',
      notes: '',
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSelectChange(event) {
    this.setState({
      rarity: event.target.value
    });
  }

  handleSubmit(event) {
    const { name, rarity, notes } = this.state;
    let newObservation = {
      id: uuidv4(),
      name,
      rarity,
      notes,
      timestamp: Date.now()
    };

    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        const coords = [ position.coords.latitude, position.coords.longitude ];
        newObservation['geolocation'] = coords;

        this.updateLocalStorage(newObservation);
      });
    }
    else {
      this.updateLocalStorage(newObservation);
    }

    event.preventDefault();
  }

  updateLocalStorage(newObservation) {
    let updatedObservationData = [ newObservation ];
    const oldObservationData = JSON.parse(localStorage.getItem('observationData'));

    if (oldObservationData) {
      updatedObservationData = [ ...oldObservationData, newObservation ];
    }

    localStorage.setItem('observationData', JSON.stringify(updatedObservationData));
    console.log("Added observation");

    this.setState({
      submitted: true
    });
  }

  render() {
    if (this.state.submitted) {
      return <Redirect to='/' />
    }

    const { name, rarity, notes } = this.state;

    return (
      <div>
        <AppBar position="sticky" color="default">
          <Toolbar>
            <Typography variant="h6" color="inherit">
              Add new observation
            </Typography>
          </Toolbar>
        </AppBar>

        <form onSubmit={this.handleSubmit}>
          <FormControl>
            <TextField
              required
              label="Name"
              name="name"
              value={name}
              onChange={this.handleInputChange}
              variant="filled"
              margin="normal" />
          </FormControl>

          <br />

          <FormControl variant="filled">
            <InputLabel htmlFor="rarity">Rarity</InputLabel>
            <Select
              value={rarity}
              onChange={this.handleSelectChange}
              input={<FilledInput name="rarity" id="rarity" />}
            >
              <MenuItem value="common">Common</MenuItem>
              <MenuItem value="rare">Rare</MenuItem>
              <MenuItem value="extemely rare">Extremely rare</MenuItem>
            </Select>
          </FormControl>

          <br />

          <FormControl variant="filled">
            <TextField
              variant="filled"
              label="Notes"
              multiline
              rows="4"
              margin="normal"
              value={notes}
              name="notes"
              onChange={this.handleInputChange}
            />
          </FormControl>

          <br />

          <FormControl>
            <Button variant="contained" color="primary" type="submit">Add</Button>
          </FormControl>
          <FormControl>
            <Button variant="contained" color="secondary" component={Link} to="/">
              Cancel
            </Button>
          </FormControl>
        </form>
      </div>
    );
  }
}

export default ObservationForm;
