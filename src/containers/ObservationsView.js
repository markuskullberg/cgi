import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Observation from '../components/Observation';

class ObservationsView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      observationData: [],
    };
  }

  componentDidMount() {
    const savedData = JSON.parse(localStorage.getItem('observationData'));
    if (savedData) {
      this.setState({
        observationData: savedData,
      });
    }
  }

  render() {
    // Sort by descending timestamp
    const sortedObservationData = [...this.state.observationData].sort(
      (a, b) => b.timestamp - a.timestamp);
    let observations = sortedObservationData.map((data, index) => (
      <Grid item xs={6} md={6} lg={3} xl={3} key={index} >
        <Observation {...data} />
      </Grid>
    ))

    return (
      <div>
        <AppBar position="sticky" color="default">
          <Toolbar>
            <Typography variant="h6" color="inherit">
              Observations
            </Typography>
            <Button
              style={{ margin: "1em" }}
              variant="contained"
              color="primary"
              component={Link}
              to="/add_observation" >
              Add new observation
            </Button>
          </Toolbar>
        </AppBar>

        <div style={{ margin: '1em' }}>
          <Grid
            container
            direction="row"
            spacing={24} >
            {observations}
          </Grid>
        </div>
      </div>
    );
  }
}

export default ObservationsView;
