import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import ObservationsView from './containers/ObservationsView';
import ObservationForm from './containers/ObservationForm';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Route exact path="/" component={ObservationsView} />
          <Route path="/add_observation" component={ObservationForm} />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
